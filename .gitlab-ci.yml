image: node:16.14.0

stages:
  - build
  - check
  - deploy
  - publish

cache:
  paths:
    - node_modules/

before_script:
    - npm install -g npm@8.5.2

build:
  stage: build
  script:
    - npm install --force --no-audit
    - npm run build
  artifacts:
    expire_in: 20 min
    paths: 
      - dist/
  only:
    - main
    - merge_requests
    - tags

sonarqube-check:
  stage: check
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  before_script: []
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - merge_requests
    - main
    - tags
  when: manual

pages:
  stage: deploy
  script:
    - mkdir public
    - mkdir public/d-map
    - mkdir public/d-editmap
    - mkdir public/d-inkmap
    - mkdir public/d-geoplateforme
    - cp -r demo/* public
    - cp -r node_modules/@descartes/d-map/dist/* public/d-map
    - cp -r node_modules/@descartes/d-editmap/dist/* public/d-editmap
    - cp -r node_modules/@descartes/d-inkmap/dist/* public/d-inkmap
    - cp -r dist/* public/d-geoplateforme
    - sed -i 's/\/@descartes\/d-map\/dist\/vendor\/d-map-required.css/d-map\/vendor\/d-map-required.css/g' public/index.html
    - sed -i 's/\/@descartes\/d-map\/dist\/vendor\/d-map-required.css/..\/..\/d-map\/vendor\/d-map-required.css/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-map\/dist\/css\/d-map.css/..\/..\/d-map\/css\/d-map.css/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-editmap\/dist\/css\/d-editmap.css/..\/..\/d-editmap\/css\/d-editmap.css/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-inkmap\/dist\/vendor\/d-inkmap-required.css/..\/..\/d-inkmap\/vendor\/d-inkmap-required.css/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-inkmap\/dist\/css\/d-inkmap.css/..\/..\/d-inkmap\/css\/d-inkmap.css/g' public/examples/*/index.html
    - sed -i 's/\/vendor\/d-geoplateforme-required.css/..\/..\/d-geoplateforme\/vendor\/d-geoplateforme-required.css/g' public/examples/*/index.html
    - sed -i 's/\/css\/d-geoplateforme.css/..\/..\/d-geoplateforme\/css\/d-geoplateforme.css/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-map\/dist\/vendor\/d-map-required.js/..\/..\/d-map\/vendor\/d-map-required.js/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-map\/dist\/d-map.js/..\/..\/d-map\/d-map.js/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-editmap\/dist\/d-editmap.js/..\/..\/d-editmap\/d-editmap.js/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-inkmap\/dist\/vendor\/d-inkmap-required.js/..\/..\/d-inkmap\/vendor\/d-inkmap-required.js/g' public/examples/*/index.html
    - sed -i 's/\/@descartes\/d-inkmap\/dist\/d-inkmap.js/..\/..\/d-inkmap\/d-inkmap.js/g' public/examples/*/index.html
    - sed -i 's/\/vendor\/d-geoplateforme-required.js/..\/..\/d-geoplateforme\/vendor\/d-geoplateforme-required.js/g' public/examples/*/index.html
    - sed -i 's/\/d-geoplateforme.js/..\/..\/d-geoplateforme\/d-geoplateforme.js/g' public/examples/*/index.html
    - npm run get-version
    - VERSION=`npm run get-version | sed '1,4d'`
    - echo $VERSION
    - sed -i 's/D-GeoPlateforme/D-GeoPlateforme '"$VERSION"'/g' public/index.html
    - sed -i 's/D-GeoPlateforme/D-GeoPlateforme '"$VERSION"'/g' public/examples/*/index.html
    - apt-get update
    - apt-get install unzip
    - curl "${CI_API_V4_URL}/projects/21592/packages/generic/descartes-deconnect/1.0.0/georef_zxy_tiles_local_0_10-1.0.0.zip" > georef_zxy_tiles_local_0_10.zip
    - unzip -qq georef_zxy_tiles_local_0_10.zip -d public/examples/commons
  artifacts:
    expire_in: 20 min
    paths:
      - public
  only:
    - main
    - tags

publish-gitlab-registry-private:
  stage: publish
  script:
    - |
      echo "
      @descartes:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/
      //${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}
      " > .npmrc
    - npm publish
  artifacts:
    expire_in: 20 min
    paths: 
      - dist/
  only:
    - tags

publish-gitlab-registry-public:
  stage: publish
  script:
    - |
      echo "
      @descartes:registry=https://${CI_SERVER_HOST}/api/v4/projects/17127/packages/npm/
      //${CI_SERVER_HOST}/api/v4/projects/17127/packages/npm/:_authToken=${CI_JOB_TOKEN}
      " > .npmrc
    - cat .npmrc
    - npm publish
  artifacts:
    expire_in: 20 min
    paths: 
      - dist/
  only:
    - tags
  when: manual

publish-npmjs-registry-public:
  stage: publish
  script:
    - |
      echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > .npmrc
    - cat .npmrc
    - npm publish --access public
  artifacts:
    expire_in: 20 min
    paths: 
      - dist/
  only:
    - tags
  when: manual