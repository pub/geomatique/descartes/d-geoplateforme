/* global MODE, Descartes, ol, Gp */

var _ = require('lodash');

var Utils = Descartes.Utils;
var EventManager = Descartes.Utils.EventManager;
var Action = Descartes.Action;

var GeoPlateformeGetAltitudeInPlace = require('../UI/' + MODE + '/GeoPlateformeGetAltitudeInPlace');
var GeoPlateformeGetAltitudeDialog = require('../UI/' + MODE + '/GeoPlateformeGetAltitudeDialog');
var Layer = require('../Model/GeoPlateformeGetAltitudeLayer');

var GeoPlateformeGetAltitudeLayerName = 'GeoPlateformeGetAltitudeLayer';
var GeoPlateformeGetAltitudeLayer = new Layer(GeoPlateformeGetAltitudeLayerName);

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Action.GeoPlateformeGetAltitudeManager
 * Classe permettant d'utiliser le service de calcul d'altitude de la Géoplateforme
 *
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *
 * }
 * (end)
 *
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'runAction' de la classe définissant la vue MVC associée (<Descartes.UI.GeoPlateformeGetAltitudeInPlace> par défaut) déclenche la méthode <doCalcul>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: defaultParams
     * {Object} Objet JSON stockant les paramètres de mise en page par défaut.
     *
     * :
     * xxx - xxx (xx par défaut).
     */
    defaultParams: {

    },

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    /**
     * Constructeur: Descartes.Action.GeoPlateformeGetAltitudeManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * params -  {Object} Objet JSON stockant les paramètres initiaux (étendus par <defaultParams>), structuré comme le modèle associé.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        Action.prototype.initialize.apply(this, arguments);
        _.extend(this.model, this.defaultParams);

        if (!_.isNil(options) && !_.isNil(options.params)) {
            _.extend(this.model, options.params);
            delete options.params;
        }

        if (_.isNil(this.renderer)) {
            if (options && options.dialog) {
                this.renderer = new GeoPlateformeGetAltitudeDialog(div, this.model, options);
            } else {
                this.renderer = new GeoPlateformeGetAltitudeInPlace(div, this.model, options);
            }
        }
        this.events = new EventManager();

        if (!this.isPresentVectorLayerGeoPlateformeGetAltitude()) {
            this.vectorLayer = GeoPlateformeGetAltitudeLayer.olLayer;
            this.olMap.addLayer(this.vectorLayer);
        } else {
            var layers = this.olMap.getLayers().getArray();
            for (var i = 0, len = layers.length; i < len; i++) {
                var layer = layers[i];
                if (layer.get('title') === GeoPlateformeGetAltitudeLayerName) {
                    this.vectorLayer = layer;
                }
            }
        }

        var that = this;

        var olStyles = defaultSymbolizers.getOlStyle(defaultSymbolizers.Descartes_Symbolizers_SelectGeoPlateformeGetAltitude);

        this.selectInteraction = new ol.interaction.Select({
            condition: ol.events.condition.pointerMove,
            layers: [this.vectorLayer],
            style: function (feature, resolution) {
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                }
            }
        });
        this.selectInteraction.on('select', function (evt) {
            if (evt.selected.length > 0) {
                this.feature = evt.selected[0];
                if (this.feature.get('id') !== 'startPoint' && this.feature.get('id') !== 'endPoint') {
                    that.clearOverlays();

                    var element = document.createElement('div');
                    element.setAttribute('id', 'locapopup');
                    element.className = 'Descartes-popup';

                    var text = '';
                    if (this.feature.get('altitude')) {
                        text += this.getMessage('RESULT_ALTITUDE') + this.feature.get('altitude') + ' m';
                    }
                    if (this.feature.get('distance')) {
                        text += '</br>' + this.getMessage('RESULT_DISTANCE') + Utils.adaptUnits(this.feature.get('distance'));
                    }
                    /*if (this.feature.get('lon')) {
                        text += '</br>' + this.getMessage('RESULT_LON') + this.feature.get('lon');
                    }
                    if (this.feature.get('lat')) {
                        text += '</br>' + this.getMessage('RESULT_LAT') + this.feature.get('lat');
                    }*/

                    var closer = document.createElement('a');
                    closer.setAttribute('href', '#');
                    closer.setAttribute('id', 'locapopup-closer');
                    closer.className = 'Descartes-popup-closer';
                    closer.onclick = function () {
                        that.olMap.removeOverlay(that.popup);
                        return false;
                    };
                    element.appendChild(closer);

                    var content = document.createElement('div');
                    content.setAttribute('id', 'locapopup-content');
                    content.innerHTML = text;

                    element.appendChild(content);

                    var extent = this.feature.getGeometry().getExtent();
                    var center = ol.extent.getCenter(extent);
                    this.popup = new ol.Overlay(({
                        id: this.feature.get('id') + '_getaltitudeoverlay',
                        element: element,
                        autoPan: true,
                        position: center
                    }));

                    that.olMap.addOverlay(this.popup);
                }
            }
        }.bind(this));

        this.olMap.addInteraction(this.selectInteraction);

        this.renderer.events.register('runAction', this, this.doCalcul);
        this.renderer.events.register('localise', this, this.localize);
        this.renderer.events.register('efface', this, this.clear);
        this.renderer.events.register('selectStartPoint', this, this.selectStartPoint);
        this.renderer.events.register('selectEndPoint', this, this.selectEndPoint);
        this.renderer.events.register('deselectStartPoint', this, this.deselectStartPoint);
        this.renderer.events.register('deselectEndPoint', this, this.deselectEndPoint);
        this.renderer.draw();
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au gestionnaire.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.mapContentChanged);
    },

    mapContentChanged: function () {
        if (!this.isPresentVectorLayerGeoPlateformeGetAltitude()) {
            this.olMap.addLayer(this.vectorLayer);
        }
    },

    /**
     * Methode: doCalcul
     * Lance le calcul d'itinéraire.
     */
    doCalcul: async function () {

        if (!this.isPresentVectorLayerGeoPlateformeGetAltitude()) {
            this.olMap.addLayer(this.vectorLayer);
        }

        this.vectorLayer.getSource().clear();
        GeoPlateformeGetAltitudeLayer.startPointFeature = null;
        GeoPlateformeGetAltitudeLayer.endPointFeature = null;
        this.model.startPoint = [this.model.startLon, this.model.startLat];
        this.model.endPoint = [this.model.endLon, this.model.endLat];
        this.selectStartPoint();
        this.selectEndPoint();

        this.features = [];

        var that = this;

        Gp.Services.getAltitude({
          positions: [
            {lon: this.model.startLon, lat: this.model.startLat},
            {lon: this.model.endLon, lat: this.model.endLat}
          ],
          outputFormat: 'json',
          sampling: 50,
          onSuccess: function (response) {
            that.model.resultats = response;
            that.adaptationsResultats();
            that.addFeaturesGeoPlateformeGetAltitudeLayer();
            that.showResults();
          },
          onFailure: function (error) {
            that.model.resultats = [];
            that.showResultsError(error);
          }
        });
    },

    showResults: function () {
        var view = this.olMap.getView();
        var extent = view.getUpdatedOptions_().extent;

        var bbox = this.features[0].getGeometry().getExtent().slice(0);
        this.features.forEach(function (feature) { ol.extent.extend(bbox, feature.getGeometry().getExtent()); });
        bbox = Descartes.Utils.extendBounds(bbox, this.olMap.getSize());
        var center = ol.extent.getCenter(bbox);

        var mapType = this.olMap.get('mapType');
        var constrainResolution = mapType === Descartes.Map.MAP_TYPES.DISCRETE;

        if (!_.isNil(extent)) {
            if (ol.extent.containsExtent(extent, bbox)) {
                view.fit(bbox, {
                    constrainResolution: constrainResolution,
                    minResolution: view.getMinResolution(),
                    maxResolution: view.getMaxResolution()
                });
               view.setCenter(center);
            }
        } else {
            view.setCenter(center);
        }

        this.renderer.showResults();
	},

    showResultsError: function (error) {
        this.renderer.showResultsError(error);
	},

    /**
     * Methode: localize
     * Localise l'adresse.
     */
    localize: function () {
        var coords = [this.model.resultats.elevations[this.model.resultatActif].x, this.model.resultats.elevations[this.model.resultatActif].y];

        var extent = this.olMap.getView().getUpdatedOptions_().extent;
        if (!_.isNil(extent)) {
            if (ol.extent.containsCoordinate(extent, coords)) {
                this.olMap.getView().setCenter(coords);
            }
        } else {
            this.olMap.getView().setCenter(coords);
        }
        var feature = this.features[this.model.resultatActif];
        this.selectInteraction.getFeatures().clear();
        this.selectInteraction.getFeatures().push(feature);
        this.selectInteraction.dispatchEvent({type: 'select', selected: [feature]});

    },

    /*
     * private
     */
    isPresentVectorLayerGeoPlateformeGetAltitude: function () {
        var layers = this.olMap.getLayers().getArray();
        for (var i = 0, len = layers.length; i < len; i++) {
            if (layers[i].get('title') === GeoPlateformeGetAltitudeLayerName) {
                return true;
            }
        }
        return false;
    },

    addFeaturesGeoPlateformeGetAltitudeLayer: function () {
        this.features = [];
        for (var i = 0, len = this.model.resultats.elevations.length; i < len; i++) {
            var result = this.model.resultats.elevations[i];
            var coords = [result.x, result.y];
            var point = new ol.geom.Point(coords);

            var pointFeature = new ol.Feature({
                id: this.id + '_' + i,
                geometry: point,
                lon: result.lon,
                lat: result.lat,
                altitude: result.z,
                distance: result.distance
            });
            this.features.push(pointFeature);
        }
        this.vectorLayer.getSource().addFeatures(this.features);
    },

    /**
     * Methode: clear
     * Suppression des résultats affichés sur la carte.
     */
    clear: function () {
        this.vectorLayer.getSource().clear();
        this.selectInteraction.getFeatures().clear();
        this.clearOverlays();
    },

    clearOverlays: function () {
        var that = this;
        this.olMap.getOverlays().forEach(function (overlay) {
            if (overlay && overlay.getId().indexOf('getaltitudeoverlay') !== -1) {
                that.olMap.removeOverlay(overlay);
            }
        });
    },

    adaptationsResultats: function () {
        //conversion coords
        for (var i = 0, len = this.model.resultats.elevations.length; i < len; i++) {
            var result = this.model.resultats.elevations[i];
            var coords = [result.lon, result.lat];
            coords = ol.proj.transform(coords, "EPSG:4326", this.olMap.getView().getProjection().getCode());
            result.x = coords[0];
            result.y = coords[1];
        }
        //calcul distance total
        var point1 = new ol.geom.Point([GeoPlateformeGetAltitudeLayer.startPointFeature.get('lon'), GeoPlateformeGetAltitudeLayer.startPointFeature.get('lat')]).transform('EPSG:4326', 'EPSG:2154');
        var point2 = new ol.geom.Point([GeoPlateformeGetAltitudeLayer.endPointFeature.get('lon'), GeoPlateformeGetAltitudeLayer.endPointFeature.get('lat')]).transform('EPSG:4326', 'EPSG:2154');
        var line = new ol.geom.LineString([point1.getCoordinates(), point2.getCoordinates()]);
        var distance = line.getLength();
        this.model.resultats.distanceTotal = distance;
        //calcul distances intermediaires
        var graphDatas = [];
        graphDatas.push({id: 0, altitude: this.model.resultats.elevations[0].z, distance: 0});
        this.model.resultats.elevations[0].distance = 0;
        for (var j = 0, jlen = this.model.resultats.elevations.length - 2; j < jlen; j++) {
            var p1 = new ol.geom.Point([this.model.resultats.elevations[j].lon, this.model.resultats.elevations[j].lat]).transform('EPSG:4326', 'EPSG:2154');
            var p2 = new ol.geom.Point([this.model.resultats.elevations[j + 1].lon, this.model.resultats.elevations[j + 1].lat]).transform('EPSG:4326', 'EPSG:2154');
            var li = new ol.geom.LineString([p1.getCoordinates(), p2.getCoordinates()]);
            var d = li.getLength();
            var graphData = {};
            graphData.id = j + 1;
            graphData.altitude = this.model.resultats.elevations[j + 1].z;
            graphData.distance = graphDatas[j].distance + d;
            graphDatas.push(graphData);
            this.model.resultats.elevations[j + 1].distance = graphData.distance;
        }
        graphDatas.push({id: this.model.resultats.elevations.length - 1, altitude: this.model.resultats.elevations[this.model.resultats.elevations.length - 1].z, distance: this.model.resultats.distanceTotal});
        this.model.resultats.elevations[this.model.resultats.elevations.length - 1].distance = this.model.resultats.distanceTotal;
        this.model.resultats.graphDatas = graphDatas;
    },

    selectStartPoint: function () {
        this.deselectStartPoint();
        for (var j = 0, jlen = this.model.startPoint.length; j < jlen; j++) {
            this.model.startPoint[j] = Number(this.model.startPoint[j]);
        }
        var startPoint = ol.proj.transform(this.model.startPoint, "EPSG:4326", this.olMap.getView().getProjection().getCode());
        var style = new ol.style.Style({
          text: new ol.style.Text({
              text: '\uf041', // fa-map-marker
              font: 'normal 18px FontAwesome',
              fill: new ol.style.Fill({color: 'black'})
          })
        });
        GeoPlateformeGetAltitudeLayer.startPointFeature = new ol.Feature({
            id: 'startPoint',
            geometry: new ol.geom.Point(startPoint),
            lon: this.model.startPoint[0],
            lat: this.model.startPoint[1]
        });
        GeoPlateformeGetAltitudeLayer.startPointFeature.setStyle(style);
        this.vectorLayer.getSource().addFeatures([GeoPlateformeGetAltitudeLayer.startPointFeature]);

    },

    selectEndPoint: function () {
        this.deselectEndPoint();
        for (var j = 0, jlen = this.model.endPoint.length; j < jlen; j++) {
            this.model.endPoint[j] = Number(this.model.endPoint[j]);
        }
        var endPoint = ol.proj.transform(this.model.endPoint, "EPSG:4326", this.olMap.getView().getProjection().getCode());
        var style = new ol.style.Style({
          text: new ol.style.Text({
              text: '\uf11e', // fa-flag-checkered
              font: 'normal 18px FontAwesome',
              fill: new ol.style.Fill({color: 'black'})
          })
        });
        GeoPlateformeGetAltitudeLayer.endPointFeature = new ol.Feature({
            id: 'endPoint',
            geometry: new ol.geom.Point(endPoint),
            lon: this.model.endPoint[0],
            lat: this.model.endPoint[1]
        });
        GeoPlateformeGetAltitudeLayer.endPointFeature.setStyle(style);
        this.vectorLayer.getSource().addFeatures([GeoPlateformeGetAltitudeLayer.endPointFeature]);

    },

    deselectStartPoint: function () {
        if (GeoPlateformeGetAltitudeLayer.startPointFeature) {
            if (this.vectorLayer.getSource().getFeatures().length > 0) {
               this.vectorLayer.getSource().removeFeature(GeoPlateformeGetAltitudeLayer.startPointFeature);
            }
            GeoPlateformeGetAltitudeLayer.startPointFeature = null;
        }
    },

    deselectEndPoint: function () {
        if (GeoPlateformeGetAltitudeLayer.endPointFeature) {
            if (this.vectorLayer.getSource().getFeatures().length > 0) {
                this.vectorLayer.getSource().removeFeature(GeoPlateformeGetAltitudeLayer.endPointFeature);
            }
            GeoPlateformeGetAltitudeLayer.endPointFeature = null;
        }
    },

    CLASS_NAME: 'Descartes.Action.GeoPlateformeGetAltitudeManager'
});
module.exports = Class;
