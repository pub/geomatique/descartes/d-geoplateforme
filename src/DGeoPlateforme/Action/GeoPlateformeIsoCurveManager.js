/* global MODE, Descartes, ol, Gp */

var _ = require('lodash');

var Utils = Descartes.Utils;
var EventManager = Descartes.Utils.EventManager;
var Action = Descartes.Action;

var GeoPlateformeIsoCurveInPlace = require('../UI/' + MODE + '/GeoPlateformeIsoCurveInPlace');
var GeoPlateformeIsoCurveDialog = require('../UI/' + MODE + '/GeoPlateformeIsoCurveDialog');
var Layer = require('../Model/GeoPlateformeIsoCurveLayer');

var GeoPlateformeIsoCurveLayerName = 'GeoPlateformeIsoCurveLayer';
var GeoPlateformeIsoCurveLayer = new Layer(GeoPlateformeIsoCurveLayerName);

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Action.GeoPlateformeIsoCurveManager
 * Classe permettant d'utiliser le service d'isochrone/isodistance de la Géoplateforme
 *
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *
 * }
 * (end)
 *
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'runAction' de la classe définissant la vue MVC associée (<Descartes.UI.GeoPlateformeIsoCurveInPlace> par défaut) déclenche la méthode <doCalcul>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: defaultParams
     * {Object} Objet JSON stockant les paramètres de mise en page par défaut.
     *
     * :
     * xxx - xxx (xx par défaut).
     */
    defaultParams: {

    },

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    /**
     * Constructeur: Descartes.Action.GeoPlateformeIsoCurveManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * params -  {Object} Objet JSON stockant les paramètres initiaux (étendus par <defaultParams>), structuré comme le modèle associé.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        Action.prototype.initialize.apply(this, arguments);
        _.extend(this.model, this.defaultParams);

        if (!_.isNil(options) && !_.isNil(options.params)) {
            _.extend(this.model, options.params);
            delete options.params;
        }

        if (_.isNil(this.renderer)) {
            if (options && options.dialog) {
                this.renderer = new GeoPlateformeIsoCurveDialog(div, this.model, options);
            } else {
                this.renderer = new GeoPlateformeIsoCurveInPlace(div, this.model, options);
            }
        }
        this.events = new EventManager();

        if (!this.isPresentVectorLayerGeoPlateformeIsoCurve()) {
            this.vectorLayer = GeoPlateformeIsoCurveLayer.olLayer;
            this.olMap.addLayer(this.vectorLayer);
        } else {
            var layers = this.olMap.getLayers().getArray();
            for (var i = 0, len = layers.length; i < len; i++) {
                var layer = layers[i];
                if (layer.get('title') === GeoPlateformeIsoCurveLayerName) {
                    this.vectorLayer = layer;
                }
            }
        }

        this.renderer.events.register('runAction', this, this.doCalcul);
        this.renderer.events.register('efface', this, this.clear);
        this.renderer.events.register('selectStartPoint', this, this.selectStartPoint);
        this.renderer.events.register('deselectStartPoint', this, this.deselectStartPoint);
        this.renderer.draw();
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au gestionnaire.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.mapContentChanged);
    },

    mapContentChanged: function () {
        if (!this.isPresentVectorLayerGeoPlateformeIsoCurve()) {
            this.olMap.addLayer(this.vectorLayer);
        }
    },

    /**
     * Methode: doCalcul
     * Lance le calcul d'itinéraire.
     */
    doCalcul: async function () {

        if (!this.isPresentVectorLayerGeoPlateformeIsoCurve()) {
            this.olMap.addLayer(this.vectorLayer);
        }

        this.vectorLayer.getSource().clear();
        GeoPlateformeIsoCurveLayer.startPointFeature = null;
        this.model.startPoint = [this.model.startLon, this.model.startLat];
        this.selectStartPoint();

        this.features = [];

        var that = this;
        var constraints = [];
        if (this.model.noToll) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "autoroute"});
        if (this.model.noBridge) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "pont"});
        if (this.model.noTunnel) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "tunnel"});

        //var resource = "bdtopo-valhalla";

        var methode = 'time';
        if (this.model.modeCalcul === 'isodistance') {
            methode = 'distance';
        }

        var reverse = false;
        if (this.model.sensParcours === 'end') {
            reverse = true;
        }

        var distance = null;
        if (this.model.isodistance) {
            distance = this.model.isodistance;
        }

        var time = 0;
        if (this.model.modeCalcul === 'isochrone') {
            time = this.model.isochrone;
        }

        Gp.Services.isoCurve({
          position: {
            x: this.model.startLon,
            y: this.model.startLat
          },
          graph: this.model.modeTransport,
          //resource: resource,
          constraints: constraints,
          methode: methode,
          time: time,
          distance: distance,
          reverse: reverse,
          onSuccess: function (response) {
            that.model.resultats = response;
            that.adaptationsResultats();
            that.addFeaturesGeoPlateformeIsoCurveLayer();
            that.showResults();
          },
          onFailure: function (error) {
            that.model.resultats = [];
            that.showResultsError(error);
          }
        });
    },

    showResults: function () {
        var view = this.olMap.getView();
        var extent = view.getUpdatedOptions_().extent;
        var bbox = this.features[0].getGeometry().getExtent();
        bbox = Descartes.Utils.extendBounds(bbox, this.olMap.getSize());
        var center = ol.extent.getCenter(bbox);

        var mapType = this.olMap.get('mapType');
        var constrainResolution = mapType === Descartes.Map.MAP_TYPES.DISCRETE;

        if (!_.isNil(extent)) {
            if (ol.extent.containsExtent(extent, bbox)) {
                view.fit(bbox, {
                    constrainResolution: constrainResolution,
                    minResolution: view.getMinResolution(),
                    maxResolution: view.getMaxResolution()
                });
               view.setCenter(center);
            }
        } else {
            view.setCenter(center);
        }

        this.renderer.showResults();
	},

    showResultsError: function (error) {
        this.renderer.showResultsError(error);
	},

    /*
     * private
     */
    isPresentVectorLayerGeoPlateformeIsoCurve: function () {
        var layers = this.olMap.getLayers().getArray();
        for (var i = 0, len = layers.length; i < len; i++) {
            if (layers[i].get('title') === GeoPlateformeIsoCurveLayerName) {
                return true;
            }
        }
        return false;
    },

    addFeaturesGeoPlateformeIsoCurveLayer: function () {
        this.features = [];
        var coords = this.model.resultats.geometry.coordinates;
        var polygon = new ol.geom.Polygon(coords);

        var polygonFeature = new ol.Feature({
            id: this.id + '_isocurvefeature',
            geometry: polygon
        });
        this.features.push(polygonFeature);
        this.vectorLayer.getSource().addFeatures(this.features);
    },

    /**
     * Methode: clear
     * Suppression des résultats affichés sur la carte.
     */
    clear: function () {
        this.vectorLayer.getSource().clear();
    },

    adaptationsResultats: function () {
        //conversion coords
        for (var i = 0, len = this.model.resultats.geometry.coordinates.length; i < len; i++) {
            var coords = this.model.resultats.geometry.coordinates[i];
            for (var j = 0, jlen = coords.length; j < jlen; j++) {
                coords[j] = ol.proj.transform(coords[j], "EPSG:4326", this.olMap.getView().getProjection().getCode());
            }
        }
    },

    selectStartPoint: function () {
        this.deselectStartPoint();
        for (var j = 0, jlen = this.model.startPoint.length; j < jlen; j++) {
            this.model.startPoint[j] = Number(this.model.startPoint[j]);
        }
        this.model.startPoint = ol.proj.transform(this.model.startPoint, "EPSG:4326", this.olMap.getView().getProjection().getCode());
        var style = new ol.style.Style({
          text: new ol.style.Text({
              text: '\uf041', // fa-map-marker
              font: 'normal 18px FontAwesome',
              fill: new ol.style.Fill({color: 'black'})
          })
        });
        GeoPlateformeIsoCurveLayer.startPointFeature = new ol.Feature({
            id: 'startPoint',
            geometry: new ol.geom.Point(this.model.startPoint)
        });
        GeoPlateformeIsoCurveLayer.startPointFeature.setStyle(style);
        this.vectorLayer.getSource().addFeatures([GeoPlateformeIsoCurveLayer.startPointFeature]);

    },

    deselectStartPoint: function () {
        if (GeoPlateformeIsoCurveLayer.startPointFeature) {
            if (this.vectorLayer.getSource().getFeatures().length > 0) {
               this.vectorLayer.getSource().removeFeature(GeoPlateformeIsoCurveLayer.startPointFeature);
            }
            GeoPlateformeIsoCurveLayer.startPointFeature = null;
        }
    },

    CLASS_NAME: 'Descartes.Action.GeoPlateformeIsoCurveManager'
});
module.exports = Class;
