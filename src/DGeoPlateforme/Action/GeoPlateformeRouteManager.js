/* global MODE, Descartes, ol, Gp */

var _ = require('lodash');

var Utils = Descartes.Utils;
var EventManager = Descartes.Utils.EventManager;
var Action = Descartes.Action;

var GeoPlateformeRouteInPlace = require('../UI/' + MODE + '/GeoPlateformeRouteInPlace');
var GeoPlateformeRouteDialog = require('../UI/' + MODE + '/GeoPlateformeRouteDialog');
var Layer = require('../Model/GeoPlateformeRouteLayer');

var GeoPlateformeRouteLayerName = 'GeoPlateformeRouteLayer';
var GeoPlateformeRouteLayer = new Layer(GeoPlateformeRouteLayerName);

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Action.GeoPlateformeRouteManager
 * Classe permettant d'utiliser le service d'itinéaire de la Géoplateforme
 *
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *
 * }
 * (end)
 *
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'runAction' de la classe définissant la vue MVC associée (<Descartes.UI.GeoPlateformeRouteInPlace> par défaut) déclenche la méthode <doCalcul>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: defaultParams
     * {Object} Objet JSON stockant les paramètres de mise en page par défaut.
     *
     * :
     * xxx - xxx (xx par défaut).
     */
    defaultParams: {

    },

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    /**
     * Constructeur: Descartes.Action.GeoPlateformeRouteManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * params -  {Object} Objet JSON stockant les paramètres initiaux (étendus par <defaultParams>), structuré comme le modèle associé.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        Action.prototype.initialize.apply(this, arguments);
        _.extend(this.model, this.defaultParams);

        if (!_.isNil(options) && !_.isNil(options.params)) {
            _.extend(this.model, options.params);
            delete options.params;
        }

        if (_.isNil(this.renderer)) {
            if (options && options.dialog) {
                this.renderer = new GeoPlateformeRouteDialog(div, this.model, options);
            } else {
                this.renderer = new GeoPlateformeRouteInPlace(div, this.model, options);
            }
        }
        this.events = new EventManager();

        if (!this.isPresentVectorLayerGeoPlateformeRoute()) {
            this.vectorLayer = GeoPlateformeRouteLayer.olLayer;
            this.olMap.addLayer(this.vectorLayer);
        } else {
            var layers = this.olMap.getLayers().getArray();
            for (var i = 0, len = layers.length; i < len; i++) {
                var layer = layers[i];
                if (layer.get('title') === GeoPlateformeRouteLayerName) {
                    this.vectorLayer = layer;
                }
            }
        }

        var that = this;

        var olStyles = defaultSymbolizers.getOlStyle(defaultSymbolizers.Descartes_Symbolizers_SelectGeoPlateformeRoute);

        this.selectInteraction = new ol.interaction.Select({
            condition: ol.events.condition.pointerMove,
            layers: [this.vectorLayer],
            style: function (feature, resolution) {
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                }
            }
        });
        this.selectInteraction.on('select', function (evt) {
            if (evt.selected.length > 0) {
                this.feature = evt.selected[0];
                if (this.feature.getGeometry().getType() !== 'Point') {
                    that.clearOverlays();

                    var element = document.createElement('div');
                    element.setAttribute('id', 'locapopup');
                    element.className = 'Descartes-popup';

                    var text = '';
                    if (this.feature.get('instruction')) {
                        text = this.feature.get('instruction');
                        text += '</br>';
                    }
                    if (this.feature.get('duration')) {
                        text += this.feature.get('duration');
                        text += '</br>';
                    }
                    if (this.feature.get('distance')) {
                        text += this.feature.get('distance');
                    }

                    var closer = document.createElement('a');
                    closer.setAttribute('href', '#');
                    closer.setAttribute('id', 'locapopup-closer');
                    closer.className = 'Descartes-popup-closer';
                    closer.onclick = function () {
                        that.olMap.removeOverlay(that.popup);
                        return false;
                    };
                    element.appendChild(closer);

                    var content = document.createElement('div');
                    content.setAttribute('id', 'locapopup-content');
                    content.innerHTML = text;

                    element.appendChild(content);

                    var extent = this.feature.getGeometry().getExtent();
                    var center = ol.extent.getCenter(extent);
                    this.popup = new ol.Overlay(({
                        id: this.feature.get('id') + '_routeoverlay',
                        element: element,
                        autoPan: true,
                        position: center
                    }));

                    that.olMap.addOverlay(this.popup);
                }
            }
        }.bind(this));


        this.olMap.addInteraction(this.selectInteraction);

        this.renderer.events.register('runAction', this, this.doCalcul);
        this.renderer.events.register('localise', this, this.localize);
        this.renderer.events.register('efface', this, this.clear);
        this.renderer.events.register('selectStartPoint', this, this.selectStartPoint);
        this.renderer.events.register('selectEndPoint', this, this.selectEndPoint);
        this.renderer.events.register('deselectStartPoint', this, this.deselectStartPoint);
        this.renderer.events.register('deselectEndPoint', this, this.deselectEndPoint);
        this.renderer.draw();
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au gestionnaire.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.mapContentChanged);
    },

    mapContentChanged: function () {
        if (!this.isPresentVectorLayerGeoPlateformeRoute()) {
            this.olMap.addLayer(this.vectorLayer);
        }
    },

    /**
     * Methode: doCalcul
     * Lance le calcul d'itinéraire.
     */
    doCalcul: async function () {

        if (!this.isPresentVectorLayerGeoPlateformeRoute()) {
            this.olMap.addLayer(this.vectorLayer);
        }

        this.vectorLayer.getSource().clear();
        GeoPlateformeRouteLayer.startPointFeature = null;
        GeoPlateformeRouteLayer.endPointFeature = null;
        this.model.startPoint = [this.model.startLon, this.model.startLat];
        this.model.endPoint = [this.model.endLon, this.model.endLat];
        this.selectStartPoint();
        this.selectEndPoint();

        this.features = [];

        var that = this;
        var constraints = [];
        if (this.model.noToll) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "autoroute"});
        if (this.model.noBridge) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "pont"});
        if (this.model.noTunnel) constraints.push({"constraintType": "banned", "key": "wayType", "operator": "=", "value": "tunnel"});

        var resource = "bdtopo-osrm";
        if (this.model.modeTransport === "pedestrian") {
          resource = "bdtopo-valhalla";
        }

        Gp.Services.route({
          startPoint: {
            x: this.model.startLon,
            y: this.model.startLat
          },
          endPoint: {
            x: this.model.endLon,
            y: this.model.endLat
          },
          /*viaPoints: [{
            x: lon2,
            y: lat2
	      }],*/
          graph: this.model.modeTransport,
          resource: resource,
          constraints: constraints,
          routePreference: this.model.modeCalcul,
          geometryInInstructions: true,
          onSuccess: function (response) {
            that.model.resultats = response;
            that.adaptationsResultats();
            that.addFeaturesGeoPlateformeRouteLayer();
            that.showResults();
          },
          onFailure: function (error) {
            that.model.resultats = [];
            that.showResultsError(error);
          }
        });
    },

    showResults: function () {
        var view = this.olMap.getView();
        var extent = view.getUpdatedOptions_().extent;
        var bbox = this.model.resultats.bbox;
        bbox = Descartes.Utils.extendBounds(bbox, this.olMap.getSize());
        var center = ol.extent.getCenter(bbox);


        var mapType = this.olMap.get('mapType');
        var constrainResolution = mapType === Descartes.Map.MAP_TYPES.DISCRETE;

        if (!_.isNil(extent)) {
            if (ol.extent.containsExtent(extent, bbox)) {
                view.fit(bbox, {
                    constrainResolution: constrainResolution,
                    minResolution: view.getMinResolution(),
                    maxResolution: view.getMaxResolution()
                });
               view.setCenter(center);
            }
        } else {
            view.setCenter(center);
        }
        this.renderer.showResults();
	},

    showResultsError: function (error) {
        this.renderer.showResultsError(error);
	},

    /**
     * Methode: localize
     * Localise l'adresse.
     */
    localize: function () {
        var coords = this.model.resultats.routeInstructions[this.model.resultatActif].geometry.coordinates[0];

        var extent = this.olMap.getView().getUpdatedOptions_().extent;
        if (!_.isNil(extent)) {
            if (ol.extent.containsCoordinate(extent, coords)) {
                this.olMap.getView().setCenter(coords);
            }
        } else {
            this.olMap.getView().setCenter(coords);
        }
        var feature = this.features[this.model.resultatActif];
        this.selectInteraction.getFeatures().clear();
        this.selectInteraction.getFeatures().push(feature);
        this.selectInteraction.dispatchEvent({type: 'select', selected: [feature]});

    },

    /*
     * private
     */
    isPresentVectorLayerGeoPlateformeRoute: function () {
        var layers = this.olMap.getLayers().getArray();
        for (var i = 0, len = layers.length; i < len; i++) {
            if (layers[i].get('title') === GeoPlateformeRouteLayerName) {
                return true;
            }
        }
        return false;
    },

    addFeaturesGeoPlateformeRouteLayer: function () {
        this.features = [];
        for (var i = 0, len = this.model.resultats.routeInstructions.length; i < len; i++) {
            var result = this.model.resultats.routeInstructions[i];
            var coords = result.geometry.coordinates;
            var line = new ol.geom.LineString(coords);

            var lineFeature = new ol.Feature({
                id: this.id + '_' + i,
                geometry: line,
                duration: result.duration,
                distance: result.distance,
                instruction: result.instruction
            });
            this.features.push(lineFeature);
        }
        this.vectorLayer.getSource().addFeatures(this.features);
    },

    /**
     * Methode: clear
     * Suppression des résultats affichés sur la carte.
     */
    clear: function () {
        this.vectorLayer.getSource().clear();
        this.selectInteraction.getFeatures().clear();
        this.clearOverlays();
    },

    clearOverlays: function () {
        var that = this;
        this.olMap.getOverlays().forEach(function (overlay) {
            if (overlay && overlay.getId().indexOf('routeoverlay') !== -1) {
                that.olMap.removeOverlay(overlay);
            }
        });
    },

    adaptationsResultats: function () {
        //conversion coords et adaptations durée distance
        for (var i = 0, len = this.model.resultats.routeInstructions.length; i < len; i++) {
            var result = this.model.resultats.routeInstructions[i];
            var coords = result.geometry.coordinates;
            for (var j = 0, jlen = coords.length; j < jlen; j++) {
                coords[j] = ol.proj.transform(coords[j], "EPSG:4326", this.olMap.getView().getProjection().getCode());
            }
            result.duration = this.getMessage('RESULT_DURATION_LABEL') + Utils.formatDuration(result.duration);
            result.distance = this.getMessage('RESULT_DISTANCE_LABEL') + Utils.adaptUnits(result.distance);
            var nb = i + 1;
            result.instruction = nb + '-' + result.instruction;
        }
        //adaptations durée distance
        this.model.resultats.totalTime = this.getMessage('RESULT_DURATION_LABEL') + Utils.formatDuration(this.model.resultats.totalTime);
        this.model.resultats.totalDistance = this.getMessage('RESULT_DISTANCE_LABEL') + Utils.adaptUnits(this.model.resultats.totalDistance);
        //conversion
        var bbox = [this.model.resultats.bbox.left, this.model.resultats.bbox.bottom, this.model.resultats.bbox.right, this.model.resultats.bbox.top];
        this.model.resultats.bbox = ol.proj.transformExtent(bbox, "EPSG:4326", this.olMap.getView().getProjection().getCode());

    },

    selectStartPoint: function () {
        this.deselectStartPoint();
        for (var j = 0, jlen = this.model.startPoint.length; j < jlen; j++) {
            this.model.startPoint[j] = Number(this.model.startPoint[j]);
        }
        this.model.startPoint = ol.proj.transform(this.model.startPoint, "EPSG:4326", this.olMap.getView().getProjection().getCode());
        var style = new ol.style.Style({
          text: new ol.style.Text({
              text: '\uf041', // fa-map-marker
              font: 'normal 18px FontAwesome',
              fill: new ol.style.Fill({color: 'black'})
          })
        });
        GeoPlateformeRouteLayer.startPointFeature = new ol.Feature({
            id: 'startPoint',
            geometry: new ol.geom.Point(this.model.startPoint)
        });
        GeoPlateformeRouteLayer.startPointFeature.setStyle(style);
        this.vectorLayer.getSource().addFeatures([GeoPlateformeRouteLayer.startPointFeature]);

    },

    selectEndPoint: function () {
        this.deselectEndPoint();
        for (var j = 0, jlen = this.model.endPoint.length; j < jlen; j++) {
            this.model.endPoint[j] = Number(this.model.endPoint[j]);
        }
        this.model.endPoint = ol.proj.transform(this.model.endPoint, "EPSG:4326", this.olMap.getView().getProjection().getCode());
        var style = new ol.style.Style({
          text: new ol.style.Text({
              text: '\uf11e', // fa-flag-checkered
              font: 'normal 18px FontAwesome',
              fill: new ol.style.Fill({color: 'black'})
          })
        });
        GeoPlateformeRouteLayer.endPointFeature = new ol.Feature({
            id: 'endPoint',
            geometry: new ol.geom.Point(this.model.endPoint)
        });
        GeoPlateformeRouteLayer.endPointFeature.setStyle(style);
        this.vectorLayer.getSource().addFeatures([GeoPlateformeRouteLayer.endPointFeature]);

    },

    deselectStartPoint: function () {
        if (GeoPlateformeRouteLayer.startPointFeature) {
            if (this.vectorLayer.getSource().getFeatures().length > 0) {
               this.vectorLayer.getSource().removeFeature(GeoPlateformeRouteLayer.startPointFeature);
            }
            GeoPlateformeRouteLayer.startPointFeature = null;
        }
    },

    deselectEndPoint: function () {
        if (GeoPlateformeRouteLayer.endPointFeature) {
            if (this.vectorLayer.getSource().getFeatures().length > 0) {
                this.vectorLayer.getSource().removeFeature(GeoPlateformeRouteLayer.endPointFeature);
            }
            GeoPlateformeRouteLayer.endPointFeature = null;
        }
    },

    CLASS_NAME: 'Descartes.Action.GeoPlateformeRouteManager'
});
module.exports = Class;
