/* global Descartes*/

var _ = require('lodash');

var Utils = Descartes.Utils;
var Button = Descartes.Button;
var MessagesConstants = require('../Messages');
var GeoPlateformeIsoCurveManager = require('../Action/GeoPlateformeIsoCurveManager');
var ExternalCallsUtils = Descartes.ExternalCallsUtils;

require('./css/GeoPlateformeIsoCurve.css');

/**
 * Class: Descartes.Button.GeoPlateformeIsoCurve
 * Classe définissant un bouton permettant d'utiliser le service d'isochrone/isodistance de la Géoplateforme.
 *
 * Hérite de:
 *  - <Descartes.Button>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'paramsChanged' de la classe <Descartes.Action.PrinterParamsManager> déclenche la méthode <actualizeParams>.
 */
var Class = Utils.Class(Button, {

    /**
     * Propriete: action
     * {<Descartes.Action.PrinterParamsManager>} Action permettant de saisir les paramètres de mise en page de l'exportation PDF.
     */
    action: null,

    /**
     * Propriete: currentParams
     * {Object} Objet JSON stockant les derniers paramètres de mise en page choisis.
     */
    currentParams: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Constructeur: Descartes.Button.GeoPlateformeIsoCurve
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     */
    initialize: function (options) {
        this.enabled = true;

        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance une action <Descartes.Action.PrinterParamsManager> avec comme interface une <Descartes.UI.GeoPlateformeIsoCurveDialog>.
     */
    execute: function () {
        if (!_.isNil(this.mapContent)) {
//            var layers = this.mapContent.getVisibleLayers().sort(function (a, b) {
//                return b.displayOrder - a.displayOrder;
//            });
//
            var options = {
                params: this.currentParams,
                dialog: true
            };

            this.action = new GeoPlateformeIsoCurveManager(null, this.olMap, options);
            this.action.setMapContent(this.mapContent);
            this.action.events.register('paramsChanged', this, this.actualizeParams);

        }
    },

    /**
     * Methode: actualizeParams
     * Met à jour les paramètres de mise en page pour une prochaine exportation.
     */
    actualizeParams: function () {
        this.currentParams = this.action.model;
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.GeoPlateformeIsoCurve'
});

module.exports = Class;
