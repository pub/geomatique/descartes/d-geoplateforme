/* global MODE */

//Action
var GeoPlateformeRouteManager = require('./Action/GeoPlateformeRouteManager');
var GeoPlateformeIsoCurveManager = require('./Action/GeoPlateformeIsoCurveManager');
var GeoPlateformeGetAltitudeManager = require('./Action/GeoPlateformeGetAltitudeManager');

//Button
var GeoPlateformeRoute = require('./Button/GeoPlateformeRoute');
var GeoPlateformeIsoCurve = require('./Button/GeoPlateformeIsoCurve');
var GeoPlateformeGetAltitude = require('./Button/GeoPlateformeGetAltitude');

//UI
var AbstractGeoPlateformeRoute = require('./UI/' + MODE + '/AbstractGeoPlateformeRoute');
var GeoPlateformeRouteInPlace = require('./UI/' + MODE + '/GeoPlateformeRouteInPlace');
var GeoPlateformeRouteDialog = require('./UI/' + MODE + '/GeoPlateformeRouteDialog');
var AbstractGeoPlateformeIsoCurve = require('./UI/' + MODE + '/AbstractGeoPlateformeRoute');
var GeoPlateformeIsoCurveInPlace = require('./UI/' + MODE + '/GeoPlateformeIsoCurveInPlace');
var GeoPlateformeIsoCurveDialog = require('./UI/' + MODE + '/GeoPlateformeIsoCurveDialog');
var AbstractGeoPlateformeGetAltitude = require('./UI/' + MODE + '/AbstractGeoPlateformeRoute');
var GeoPlateformeGetAltitudeInPlace = require('./UI/' + MODE + '/GeoPlateformeGetAltitudeInPlace');
var GeoPlateformeGetAltitudeDialog = require('./UI/' + MODE + '/GeoPlateformeGetAltitudeDialog');

//Model
var GeoPlateformeRouteLayer = require('./Model/GeoPlateformeRouteLayer');
var GeoPlateformeIsoCurveLayer = require('./Model/GeoPlateformeIsoCurveLayer');
var GeoPlateformeGetAltitudeLayer = require('./Model/GeoPlateformeGetAltitudeLayer');

//Utils
var UtilsGeoPlateforme = require('./Utils/UtilsGeoPlateforme');

var Messages = require('./Messages');
var Symbolizers = require('./Symbolizers');

var DGeoPlateforme = {
	Action: {GeoPlateformeRouteManager: GeoPlateformeRouteManager, GeoPlateformeIsoCurveManager: GeoPlateformeIsoCurveManager, GeoPlateformeGetAltitudeManager: GeoPlateformeGetAltitudeManager},
	Button: {GeoPlateformeRoute: GeoPlateformeRoute, GeoPlateformeIsoCurve: GeoPlateformeIsoCurve, GeoPlateformeGetAltitude: GeoPlateformeGetAltitude},
	Layer: {GeoPlateformeRouteLayer: GeoPlateformeRouteLayer, GeoPlateformeIsoCurveLayer: GeoPlateformeIsoCurveLayer, GeoPlateformeGetAltitudeLayer: GeoPlateformeGetAltitudeLayer},
	Messages: Messages,
	Symbolizers: Symbolizers,
	UI: {AbstractGeoPlateformeRoute: AbstractGeoPlateformeRoute, GeoPlateformeRouteInPlace: GeoPlateformeRouteInPlace, GeoPlateformeRouteDialog: GeoPlateformeRouteDialog, AbstractGeoPlateformeIsoCurve: AbstractGeoPlateformeIsoCurve, GeoPlateformeIsoCurveInPlace: GeoPlateformeIsoCurveInPlace, GeoPlateformeIsoCurveDialog: GeoPlateformeIsoCurveDialog, AbstractGeoPlateformeGetAltitude: AbstractGeoPlateformeGetAltitude, GeoPlateformeGetAltitudeInPlace: GeoPlateformeGetAltitudeInPlace, GeoPlateformeGetAltitudeDialog: GeoPlateformeGetAltitudeDialog},
	Utils: {UtilsGeoPlateforme: UtilsGeoPlateforme}
};

module.exports = DGeoPlateforme;
