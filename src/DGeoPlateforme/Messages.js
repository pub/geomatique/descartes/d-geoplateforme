var _ = require('lodash');

var messages = {

    Descartes_Messages_Action_GeoPlateformeRouteManager: {
        RESULT_DURATION_LABEL: 'Durée : ',
        RESULT_DISTANCE_LABEL: 'Distance : '
    },

    Descartes_Messages_Button_GeoPlateformeRoute: {
        TITLE: 'Calcul d\'itinéraire'
    },
    Descartes_Forms_GeoPlateformeRoute: {
        ERRORS_LIST: 'Le calcul ne peut être déclenché car les erreurs suivantes ont été rencontrées :',
        START_LABEL: 'Départ',
        START_PLACEHOLDER: 'saisir une adresse de départ',
        START_ERROR: 'saisie de l\'adresse de départ non valide',
        STARTLON_ERROR: 'Départ - x : valeur incorrecte',
        STARTLAT_ERROR: 'Départ - y : valeur incorrecte',
        START_LON_LABEL: 'x:',
        START_LAT_LABEL: 'y:',
        END_LABEL: 'Arrivée',
        END_PLACEHOLDER: 'saisir une adresse d\'arrivée',
        END_ERROR: 'saisie de l\'adresse d\'arrivée non valide',
        ENDLON_ERROR: 'Arrivée - x : valeur incorrecte',
        ENDLAT_ERROR: 'Arrivée - y : valeur incorrecte',
        END_LON_LABEL: 'x:',
        END_LAT_LABEL: 'y:',
        EXCLUDE_LABEL: 'Eviter',
        TOLL_TEXT: 'péages',
        BRIDGE_TEXT: 'ponts',
        TUNNEL_TEXT: 'tunnels',
        MODE_TRANSPORT_LABEL: 'Transport',
        CAR_MODE_TRANSPORT: 'Voiture',
        PIETON_MODE_TRANSPORT: 'Piéton',
        MODE_CALCUL_LABEL: 'Itinéraire',
        FASTEST_MODE_CALCUL: 'le plus rapide',
        SHORTEST_MODE_CALCUL: 'le plus court',
        RUN_BUTTON_MESSAGE: 'Calculer',
        ERASE_BUTTON_MESSAGE: 'Effacer',
        WAIT_MESSAGE: 'Calcul en cours',
        EMPTY_MESSAGE: 'Pas d\'itinétaire possible',
        FAILURE_GP_SERVICE: 'Géoplateforme service autoComplete error',
        RESULT_ETAPE_NB_LABEL: 'Itinéraire : ',
        RESULT_ETAPE_EMPTY: 'Pas d\'étapes',
        RESULT_ETAPE: 'étape'
    },

    Descartes_Messages_UI_GeoPlateformeRouteInPlace: {
        TITLE_MESSAGE: 'Calcul d\'itinéraire'
    },
    Descartes_Messages_UI_GeoPlateformeRouteDialog: {
        DIALOG_TITLE: 'Calcul d\'itinéraire',
        SUBMIT_BUTTON: 'Fermer'
    },

    Descartes_Messages_Action_GeoPlateformeIsoCurveManager: {
    },

    Descartes_Messages_Button_GeoPlateformeIsoCurve: {
        TITLE: 'Calcul d\'isochrone/isodistance'
    },
    Descartes_Forms_GeoPlateformeIsoCurve: {
        ERRORS_LIST: 'Le calcul ne peut être déclenché car les erreurs suivantes ont été rencontrées :',
        START_LABEL: 'Départ',
        START_PLACEHOLDER: 'saisir une adresse de départ',
        START_ERROR: 'saisie de l\'adresse de départ non valide',
        STARTLON_ERROR: 'Départ - x : valeur incorrecte',
        STARTLAT_ERROR: 'Départ - y : valeur incorrecte',
        START_LON_LABEL: 'x:',
        START_LAT_LABEL: 'y:',
        EXCLUDE_LABEL: 'Eviter',
        TOLL_TEXT: 'péages',
        BRIDGE_TEXT: 'ponts',
        TUNNEL_TEXT: 'tunnels',
        SENS_LABEL: 'Sens',
        SENS_START: 'Départ',
        SENS_END: 'Arrivée',
        MODE_TRANSPORT_LABEL: 'Transport',
        CAR_MODE_TRANSPORT: 'Voiture',
        PIETON_MODE_TRANSPORT: 'Piéton',
        MODE_CALCUL_LABEL: 'Calcul',
        ISOCHRONE_MODE_CALCUL: 'Isochrone',
        ISODISTANCE_MODE_CALCUL: 'Isodistance',
        ISOCHRONE_LABEL: 'Temps',
        ISODISTANCE_LABEL: 'Distance',
        RUN_BUTTON_MESSAGE: 'Calculer',
        ERASE_BUTTON_MESSAGE: 'Effacer',
        WAIT_MESSAGE: 'Calcul en cours',
        EMPTY_MESSAGE: 'Calcul impossible',
        FAILURE_GP_SERVICE: 'Géoplateforme service autoComplete error'
    },

    Descartes_Messages_UI_GeoPlateformeIsoCurveInPlace: {
        TITLE_MESSAGE: 'Calcul d\'isochrone/isodistance'
    },
    Descartes_Messages_UI_GeoPlateformeIsoCurveDialog: {
        DIALOG_TITLE: 'Calcul d\'isochrone/isodistance',
        SUBMIT_BUTTON: 'Fermer'
    },

    Descartes_Messages_Action_GeoPlateformeGetAltitudeManager: {
        RESULT_LON: 'Lon : ',
        RESULT_LAT: 'Lat : ',
        RESULT_ALTITUDE: 'Altitude : ',
        RESULT_DISTANCE: 'Distance : '
    },

    Descartes_Messages_Button_GeoPlateformeGetAltitude: {
        TITLE: 'Profil altimétrique'
    },
    Descartes_Forms_GeoPlateformeGetAltitude: {
        ERRORS_LIST: 'Le calcul ne peut être déclenché car les erreurs suivantes ont été rencontrées :',
        START_LABEL: 'Départ',
        START_PLACEHOLDER: 'saisir une adresse de départ',
        START_ERROR: 'saisie de l\'adresse de départ non valide',
        STARTLON_ERROR: 'Départ - x : valeur incorrecte',
        STARTLAT_ERROR: 'Départ - y : valeur incorrecte',
        START_LON_LABEL: 'x:',
        START_LAT_LABEL: 'y:',
        END_LABEL: 'Arrivée',
        END_PLACEHOLDER: 'saisir une adresse d\'arrivée',
        END_ERROR: 'saisie de l\'adresse d\'arrivée non valide',
        ENDLON_ERROR: 'Arrivée - x : valeur incorrecte',
        ENDLAT_ERROR: 'Arrivée - y : valeur incorrecte',
        END_LON_LABEL: 'x:',
        END_LAT_LABEL: 'y:',
        RESULT_LON: 'Lon : ',
        RESULT_LAT: 'Lat : ',
        RESULT_ALTITUDE: 'Altitude : ',
        RUN_BUTTON_MESSAGE: 'Calculer',
        ERASE_BUTTON_MESSAGE: 'Effacer',
        WAIT_MESSAGE: 'Calcul en cours',
        EMPTY_MESSAGE: 'Calcul impossible',
        FAILURE_GP_SERVICE: 'Géoplateforme service autoComplete error'
    },

    Descartes_Messages_UI_GeoPlateformeGetAltitudeInPlace: {
        TITLE_MESSAGE: 'Profil altimétrique'
    },
    Descartes_Messages_UI_GeoPlateformeGetAltitudeDialog: {
        DIALOG_TITLE: 'Profil altimétrique',
        SUBMIT_BUTTON: 'Fermer'
    }

};

messages.Descartes_Messages_UI_GeoPlateformeRouteInPlace = _.extend(messages.Descartes_Messages_UI_GeoPlateformeRouteInPlace, messages.Descartes_Forms_GeoPlateformeRoute);
messages.Descartes_Messages_UI_GeoPlateformeRouteDialog = _.extend(messages.Descartes_Messages_UI_GeoPlateformeRouteDialog, messages.Descartes_Forms_GeoPlateformeRoute);
messages.Descartes_Messages_UI_GeoPlateformeIsoCurveInPlace = _.extend(messages.Descartes_Messages_UI_GeoPlateformeIsoCurveInPlace, messages.Descartes_Forms_GeoPlateformeIsoCurve);
messages.Descartes_Messages_UI_GeoPlateformeIsoCurveDialog = _.extend(messages.Descartes_Messages_UI_GeoPlateformeIsoCurveDialog, messages.Descartes_Forms_GeoPlateformeIsoCurve);
messages.Descartes_Messages_UI_GeoPlateformeGetAltitudeInPlace = _.extend(messages.Descartes_Messages_UI_GeoPlateformeGetAltitudeInPlace, messages.Descartes_Forms_GeoPlateformeGetAltitude);
messages.Descartes_Messages_UI_GeoPlateformeGetAltitudeDialog = _.extend(messages.Descartes_Messages_UI_GeoPlateformeGetAltitudeDialog, messages.Descartes_Forms_GeoPlateformeGetAltitude);

module.exports = messages;
