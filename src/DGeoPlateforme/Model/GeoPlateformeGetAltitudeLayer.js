/* global Descartes, ol */

var _ = require('lodash');

var Utils = Descartes.Utils;

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Layer.GeoPlateformeGetAltitudeLayer
 * Classe permettant de créer une couche vecteur afin d'afficher sur la carte les résultats du service de calcul d'altitude.
 *
 * :
 * Voir <Descartes.Action.GeoPlateformeGetAltitudeManager> pour l'utilisation de cette classe.
 */
var Layer = Utils.Class({

    styles: {
        defaultStyle: null,
        selectStyle: null
    },

    styleMap: null,

    /**
     * Propriete: title
     * {String} Titre de la couche vecteur.
     */
    title: null,

    olLayer: null,
    /**
     * Constructeur: Descartes.Layer.GeoPlateformeGetAltitudeLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Titre de la couche vecteur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, options) {

        this.title = title;

        if (!_.isNil(options)) {
            _.extend(defaultSymbolizers.Descartes_Symbolizers_DefaultGeoPlateformeGetAltitude, options.defaultSymbolizer);
            delete options.defaultSymbolizer;
            _.extend(defaultSymbolizers.Descartes_Symbolizers_SelectGeoPlateformeGetAltitude, options.selectSymbolizer);
            delete options.selectSymbolizer;
        }

        this.features = new ol.Collection();
        this.olLayer = new ol.layer.Vector({
            title: this.title,
            source: new ol.source.Vector({
                features: this.features
            }),
            style: function (feature, resolution) {
                var olStyles = defaultSymbolizers.getOlStyle(defaultSymbolizers.Descartes_Symbolizers_DefaultGeoPlateformeGetAltitude);
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                }
            }
        });
    },

    CLASS_NAME: 'Descartes.Layer.GeoPlateformeGetAltitudeLayer'
});

module.exports = Layer;
