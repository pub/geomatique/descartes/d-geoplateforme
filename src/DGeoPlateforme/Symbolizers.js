/* global Descartes */

var symbolizers = Descartes.Symbolizers;

symbolizers.Descartes_Symbolizers_DefaultGeoPlateformeRoute = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_SelectGeoPlateformeRoute = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#ee9900',
        fillColor: '#ee9900',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_DefaultGeoPlateformeIsoCurve = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_SelectGeoPlateformeIsoCurve = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#ee9900',
        fillColor: '#ee9900',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_DefaultGeoPlateformeGetAltitude = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_SelectGeoPlateformeGetAltitude = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 8,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#ee9900',
        fillColor: '#ee9900',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

module.exports = symbolizers;
