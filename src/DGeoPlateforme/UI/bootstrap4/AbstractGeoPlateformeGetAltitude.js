/* global Descartes, Gp, Chart*/

var $ = require('jquery');

var Utils = Descartes.Utils;
var UI = Descartes.UI;

var _ = require('lodash');
var GeoPlateformeGetAltitudeTemplate = require('./templates/GeoPlateformeGetAltitude.ejs');

require('../css/SimpleUIs.css');

/**
 * Class: Descartes.UI.AbstractGeoPlateformeGetAltitude
 * Classe "abstraite" proposant la saisie des paramètres pour le service de calcul d'altitude de la GéoPlateforme'.
 *
 * :
 * Le formulaire de saisie est constitué de zones ....
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Classes dérivées:
 *  - <Descartes.UI.GeoPlateformeGetAltitudeDialog>
 *  - <Descartes.UI.GeoPlateformeGetAltitudeInPlace>
 */
var Class = Utils.Class(UI, {
    /**
     * Propriete: errors
     * {Array(String)} Liste des erreurs rencontrés lors des contrôles de surface.
     */
    errors: null,

    /**
     * Propriete: form
     * {DOMElement} Elément DOM correspondant au formulaire de saisie.
     */
    form: null,

    panelCss: null,

    EVENT_TYPES: ['runAction', "localise", "efface", "selectStartPoint", "selectEndPoint", "deselectStartPoint", "deselectEndPoint"],

    /**
     * Constructeur: Descartes.UI.AbstractGeoPlateformeGetAltitude
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.GeoPlateformeGetAltitudeManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, paramsModel, options) {
        this.panelCss = Descartes.UIBootstrap4Options.panelCss;
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        UI.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: populateForm
     * Construit les zones de saisie du formulaire.
     */
    populateForm: function () {
        return GeoPlateformeGetAltitudeTemplate(_.extend({
            id: this.id,
            startLabel: this.getMessage('START_LABEL'),
            startPlaceHolder: this.getMessage('START_PLACEHOLDER'),
            startPointLon: this.getMessage('START_LON_LABEL'),
            startPointLat: this.getMessage('START_LAT_LABEL'),
            endLabel: this.getMessage('END_LABEL'),
            endPlaceHolder: this.getMessage('END_PLACEHOLDER'),
            endPointLon: this.getMessage('END_LON_LABEL'),
            endPointLat: this.getMessage('END_LAT_LABEL'),
            runActionButtonText: this.getMessage('RUN_BUTTON_MESSAGE'),
            eraseActionButtonText: this.getMessage('ERASE_BUTTON_MESSAGE'),
            waitMessage: this.getMessage("WAIT_MESSAGE"),
            btnCss: this.btnCss
        }, this.model));
    },

    registerEvents: function (id) {
        var container = document.getElementById(id);

        container.querySelector("button[name='runAction']")
       .addEventListener('click', event => {
            event.preventDefault();
            this.clear();
            this.result = Utils.serializeFormArrayToJson($(event.target.form).serializeArray());
            this.done();
        });

        container.querySelector("button[name='eraseAction']")
       .addEventListener('click', event => {
            event.preventDefault();
            this.clear();
        });

        container.querySelector("input[id='input-startdatalist']").autocomplete = 'off';
        container.querySelector("input[id='input-startdatalist']")
        .addEventListener("keyup", (event) => {
            if (event.which && event.which !== 13) {
                var form = document.getElementById(this.formId);
                form.querySelector("input[name='startLon']").value = "";
                form.querySelector("input[name='startLat']").value = "";
                     form.querySelector("input[name='startLon']").className = form.querySelector("input[name='startLon']").className.replace(/ok/g, 'ko');
                     form.querySelector("input[name='startLat']").className = form.querySelector("input[name='startLat']").className.replace(/ok/g, 'ko');
                form.querySelector("label[id='labelStartPointLon']").className = form.querySelector("label[id='labelStartPointLon']").className.replace(/ok/g, 'ko');
                form.querySelector("label[id='labelStartPointLat']").className = form.querySelector("label[id='labelStartPointLat']").className.replace(/ok/g, 'ko');
                this.events.triggerEvent('deselectStartPoint');
                if (event.which <= 90 && event.which >= 48 || event.which >= 96 && event.which <= 105) {
                    this.autoComplete(event.target.value, this.id + '-list-startzone');
                }
            }
        });

        container.querySelector("input[list='" + this.id + "-list-startzone']")
        .addEventListener("input", (event) => {
             var input = event.target;
             var val = input.value;
             var list = input.getAttribute('list');
             var options = document.getElementById(list).childNodes;

             for(var i = 0; i < options.length; i++) {
                 if(options[i].value === val) {
                     var form = document.getElementById(this.formId);
                     form.querySelector("input[name='startLon']").value = options[i].attributes["data-lon"].value;
                     form.querySelector("input[name='startLat']").value = options[i].attributes["data-lat"].value;
                     form.querySelector("input[name='startLon']").className = form.querySelector("input[name='startLon']").className.replace(/ko/g, 'ok');
                     form.querySelector("input[name='startLat']").className = form.querySelector("input[name='startLat']").className.replace(/ko/g, 'ok');
                     form.querySelector("label[id='labelStartPointLon']").className = form.querySelector("label[id='labelStartPointLon']").className.replace(/ko/g, 'ok');
                     form.querySelector("label[id='labelStartPointLat']").className = form.querySelector("label[id='labelStartPointLat']").className.replace(/ko/g, 'ok');
                     this.model.startPoint = [options[i].attributes["data-lon"].value, options[i].attributes["data-lat"].value];
                     this.events.triggerEvent('selectStartPoint');
                     break;
                 }
             }
        });

        container.querySelector("input[id='input-enddatalist']").autocomplete = 'off';
        container.querySelector("input[id='input-enddatalist']")
        .addEventListener("keyup", (event) => {
            if (event.which && event.which !== 13) {
                var form = document.getElementById(this.formId);
                form.querySelector("input[name='endLon']").value = "";
                form.querySelector("input[name='endLat']").value = "";
                form.querySelector("input[name='endLon']").className = form.querySelector("input[name='endLon']").className.replace(/ok/g, 'ko');
                form.querySelector("input[name='endLat']").className = form.querySelector("input[name='endLat']").className.replace(/ok/g, 'ko');
                form.querySelector("label[id='labelEndPointLon']").className = form.querySelector("label[id='labelEndPointLon']").className.replace(/ok/g, 'ko');
                form.querySelector("label[id='labelEndPointLat']").className = form.querySelector("label[id='labelEndPointLat']").className.replace(/ok/g, 'ko');
                this.events.triggerEvent('deselectEndPoint');
                if (event.which <= 90 && event.which >= 48 || event.which >= 96 && event.which <= 105) {
                    this.autoComplete(event.target.value, this.id + '-list-endzone');
                }
            }
        });

        container.querySelector("input[list='" + this.id + "-list-endzone']")
        .addEventListener("input", (event) => {
             var input = event.target;
             var val = input.value;
             var list = input.getAttribute('list');
             var options = document.getElementById(list).childNodes;

             for(var i = 0; i < options.length; i++) {
                 if(options[i].value === val) {
                     var form = document.getElementById(this.formId);
                     form.querySelector("input[name='endLon']").value = options[i].attributes["data-lon"].value;
                     form.querySelector("input[name='endLat']").value = options[i].attributes["data-lat"].value;
                     form.querySelector("input[name='endLon']").className = form.querySelector("input[name='endLon']").className.replace(/ko/g, 'ok');
                     form.querySelector("input[name='endLat']").className = form.querySelector("input[name='endLat']").className.replace(/ko/g, 'ok');
                     form.querySelector("label[id='labelEndPointLon']").className = form.querySelector("label[id='labelEndPointLon']").className.replace(/ko/g, 'ok');
                     form.querySelector("label[id='labelEndPointLat']").className = form.querySelector("label[id='labelEndPointLat']").className.replace(/ko/g, 'ok');
                     this.model.endPoint = [options[i].attributes["data-lon"].value, options[i].attributes["data-lat"].value];
                     this.events.triggerEvent('selectEndPoint');
                     break;
                 }
             }
        });

        container.querySelector("input[name='startLon']")
        .addEventListener("change", (event) => {
            this.model.startLon = event.target.value;
        });

        container.querySelector("input[name='startLat']")
        .addEventListener("change", (event) => {
            this.model.startLat = event.target.value;
        });

        container.querySelector("input[name='endLon']")
        .addEventListener("change", (event) => {
            this.model.endLon = event.target.value;
        });

        container.querySelector("input[name='endLat']")
        .addEventListener("change", (event) => {
            this.model.endLat = event.target.value;
        });

    },

    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'runAction', ou affiche les messages d'erreur rencontrés.
     */
    done: function () {
        if (this.validateDatas()) {
            this.showWaitMessage();
            this.events.triggerEvent('runAction');
        } else {
            this.showErrors();
        }
    },

    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * :
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * :
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function () {
        this.errors = [];
        if (!document.getElementById(this.formId).checkValidity()) {
            document.getElementById(this.formId).reportValidity();
            return false;
        } else if ((_.isEmpty(this.result.startLon) && _.isEmpty(this.result.startLat)) || (_.isEmpty(this.result.endLon) && _.isEmpty(this.result.endLat))) {
            if (_.isEmpty(this.result.startLon) && _.isEmpty(this.result.startLat)) {
               this.errors.push(this.getMessage('START_ERROR'));
            }
            if (_.isEmpty(this.result.endLon) && _.isEmpty(this.result.endLat)) {
               this.errors.push(this.getMessage('END_ERROR'));
            }
            return false;
        }

        this.model.startLon = Number(this.result.startLon.replace(",", "."));
        this.model.startLat = Number(this.result.startLat.replace(",", "."));
        this.model.endLon = Number(this.result.endLon.replace(",", "."));
        this.model.endLat = Number(this.result.endLat.replace(",", "."));
        if (isNaN(this.model.startLon)) {
            this.errors.push(this.getMessage('STARTLON_ERROR'));
        }
        if (isNaN(this.model.startLat)) {
            this.errors.push(this.getMessage('STARTLAT_ERROR'));
        }
        if (isNaN(this.model.endLon)) {
            this.errors.push(this.getMessage('ENDLON_ERROR'));
        }
        if (isNaN(this.model.endLat)) {
            this.errors.push(this.getMessage('ENDLAT_ERROR'));
        }

        return (this.errors.length === 0);
    },

    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {

            var errorsMessage = '<ul>';
            for (var i = 0, len = this.errors.length; i < len; i++) {
                errorsMessage += '<li>' + this.errors[i] + '</li>';
            }
            errorsMessage += '</ul>';
            var dialog = new Descartes.UI.ConfirmDialog({
                id: this.id + '_confirmDialog',
                title: this.getMessage('ERRORS_LIST'),
                message: errorsMessage,
                type: 'default'
            });
            dialog.open();
        }
    },

    showResults: function () {
        var that = this;
        this.closeWaitMessage();

        var div = document.getElementById(this.id);
        var result = div.querySelector("div[id='runActionResult']");
        Utils.deleteChild(result);

        /*var header = document.createElement('span');
        header.id = 'headerRunActionResultats';
        header.innerHTML = 'Distance totale: ' + Utils.adaptUnits(this.model.resultats.distanceTotal);
        result.append(header);

        if (this.model.resultats && this.model.resultats.graphDatas && this.model.resultats.graphDatas.length > 0) {
            var list = document.createElement('div');
            list.class = 'list-group';
            result.append(list);
            _.each(this.model.resultats.graphDatas, function (resultat, index) {
                var text = 'id: ' + resultat.id;
                text += '</br>' + 'distance: ' + Utils.adaptUnits(resultat.distance);
                text += '</br>' + that.getMessage('RESULT_ALTITUDE') + resultat.altitude;
                var item = document.createElement('a');
                item.href = '#';
                item.setAttribute('data-index', index);
                item.className = 'list-group-item';
                item.onclick = function () {
                    that.geocode(this);
                };
                item.innerHTML = text;
                list.append(item);
            });
        }

        result.style.display = "block";*/

        this.displayChart();
	},

    showResultsError: function (error) {
        this.closeWaitMessage();
        this.showErrorMessage(error);
    },

    /**
     * Methode: showWaitMessage
     * Affiche le message d'attente.
     */
    showWaitMessage: function () {
        var div = document.getElementById(this.id);
        var waitDiv = div.querySelector("div[id='runActionWait']");
        waitDiv.style.display = "block";
    },

    /**
     * Methode: closeWaitMessage
     * Ferme hidele message d'attente.
     */
    closeWaitMessage: function () {
        var div = document.getElementById(this.id);
        var waitDiv = div.querySelector("div[id='runActionWait']");
        waitDiv.style.display = "none";
    },

    showErrorMessage: function (error) {
        var div = document.getElementById(this.id);
        var errorDiv = div.querySelector("div[id='runActionError']");
        var message = "";
        if (error && error.message) {
            if (error.message === "The response of the service is empty") {
                message += this.getMessage('EMPTY_MESSAGE');
            } else {
                message += "ERREUR : " + error.message;
            }
        }
        errorDiv.innerHTML = message;
        errorDiv.style.display = "block";

    },

    closeErrorMessage: function () {
        var div = document.getElementById(this.id);
        var errorDiv = div.querySelector("div[id='runActionError']");
        errorDiv.style.display = "none";
    },

    clear: function () {
        var div = document.getElementById(this.id);
        var result = div.querySelector("div[id='runActionResult']");
        Utils.deleteChild(result);
        result.style.display = "none";

        this.closeErrorMessage();
        if (this.chart) this.chart.destroy();

        this.events.triggerEvent("efface");
    },

    geocode: function (elt) {
        this.model.resultatActif = elt.getAttribute('data-index');
        this.events.triggerEvent("localise");
    },

    autoComplete: async function (text, idZone) {
        var that = this;

        var div = document.getElementById(this.id);
        var list = div.querySelector("datalist[id='" + idZone + "']");
        Utils.deleteChild(list);

        this.idZone = idZone;
        this.text = text;
        if (text.length > 2) {

          Gp.Services.autoComplete({
            text: text,
            type: ["StreetAddress"],
            territory: '',
            maximumResponses: 10,
            onSuccess: function (response) {
               that.showListAutoComplete(response, that.idZone);
            },
            onFailure: function (response) {
                that.failureAutoComplete(response, that.idZone);
            }
          });
        }
    },

    failureAutoComplete: function (response, idZone) {
        var message = this.getMessage('FAILURE_GP_SERVICE');
        if (response.message) {
            message = this.text + ' ' + response.message;
        } else {
            alert(message);
        }
        var list = $('#' + idZone);
        list.empty();
        list.append('<option value="' + message + '"></option>');
    },

    showListAutoComplete: function (response, idZone) {

        var list = $('#' + this.idZone);
        list.empty();

        if (response && response.suggestedLocations && response.suggestedLocations.length > 0) {
            _.each(response.suggestedLocations, function (suggestedLocation, index) {
                var text = suggestedLocation.fullText;
                var lon = suggestedLocation.position.x;
                var lat = suggestedLocation.position.y;
                list.append('<option data-index=' + index + ' data-lon=' + lon + ' data-lat=' + lat + ' value="' + text + '"></option>');
            });
        }

	},

    displayChart: function () {
        var that = this;

        var ctx = document.getElementById(this.formId).querySelector("canvas[id='chart']");

        var labels = [];
        var data = [];
        for (var i = 0, len = this.model.resultats.graphDatas.length; i < len; i++) {
            labels.push(Utils.adaptUnits(this.model.resultats.graphDatas[i].distance));
            data.push(this.model.resultats.graphDatas[i].altitude);
        }

        this.chart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: labels,
            datasets: [{
              label: 'Altitude',
              data: data,
              borderWidth: 1//,
              //borderColor: "red",
              //backgroundColor: "red",
              //pointStyle: 'rectRot',
              //pointBorderColor: 'rgb(0, 0, 0)'
              //pointRadius: 5
            }]
          },
          options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
              x: {
                title: {
                  display: true,
                  text: 'Distance'
                }
             },
              y: {
                //beginAtZero: true,
                //min: 0,
                suggestedMax: 50,
                title: {
                  display: true,
                  text: 'Altitude (m)'
                }
              }
            },
            plugins: {
                title: {
                    display: true,
                    text: 'Profil altimétrique'
                },
                legend: {
                    display: false,
                    labels: {
                        usePointStyle: true
                    }
                },
                tooltip: {
                    callbacks: {
                        label: function (context) {
                            let label = context.dataset.label || '';

                            if (label) {
                                label += ': ';
                            }
                            if (context.parsed.y !== null) {
                                label += context.parsed.y + ' m';
                            }
                            that.model.resultatActif = context.dataIndex;
                            that.events.triggerEvent("localise");
                            return label;
                        }
                    }
                }
            }
          }
        });
	},


    CLASS_NAME: 'Descartes.UI.AbstractGeoPlateformeGetAltitude'
});

module.exports = Class;
