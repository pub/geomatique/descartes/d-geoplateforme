/* global Descartes */

var $ = require('jquery');
var _ = require('lodash');

var Utils = Descartes.Utils;
var AbstractInPlace = Descartes.UI.AbstractInPlace;

var AbstractGeoPlateformeGetAltitude = require('./AbstractGeoPlateformeGetAltitude');

/**
 * Class: Descartes.UI.GeoPlateformeGetAltitudeInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie des paramètres pour le service de calcul d'altitude de la GéoPlateforme'.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractInPlace>
 *
 * Evénements déclenchés:
 * choosed - Les paramètres sont saisis et valides.
 */
var Class = Utils.Class(AbstractInPlace, AbstractGeoPlateformeGetAltitude, {

    /**
     * Constructeur: Descartes.UI.GeoPlateformeGetAltitudeInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.GeoPlateformeGetAltitudeManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des paramètres d'impression.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceJQueryUI>).
     */
    initialize: function (div, paramsModel, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
        AbstractGeoPlateformeGetAltitude.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des paramètres de mise en page.
     */
    draw: function () {
        this.formId = this.id + '_form';
        var content = '<div id="' + this.id + '" class="DescartesUIGpGetAltitude">';
        content += '<form id="' + this.formId + '" class="form-horizontal">';
        content += this.populateForm();
        content += '</form>';
        content += '</div>';

        this.renderPanel(content);

        this.registerEvents(this.formId);

    },

    CLASS_NAME: 'Descartes.UI.GeoPlateformeGetAltitudeInPlace'
});

module.exports = Class;
