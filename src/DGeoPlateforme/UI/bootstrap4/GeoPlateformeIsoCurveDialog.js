/* global Descartes */

var $ = require('jquery');
var _ = require('lodash');

var Utils = Descartes.Utils;
var FormDialog = Descartes.UI.FormDialog;

var AbstractGeoPlateformeIsoCurve = require('./AbstractGeoPlateformeIsoCurve');

/**
 * Class: Descartes.UI.GeoplateformeIsoCurveDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie des paramètres pour le service d'isocurve de la GéoPlateforme'.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractGeoPlateformeIsoCurve>
 *
 * Evénements déclenchés:
 * choosed - Les paramètres sont saisis et valides.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(AbstractGeoPlateformeIsoCurve, {

    /**
     * Constructeur: Descartes.UI.GeoPlateformeIsoCurveDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - null.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.GeoPlateformeIsoCurveManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, paramsModel, options) {
        _.extend(this, options);
        AbstractGeoPlateformeIsoCurve.prototype.initialize.apply(this, [div, paramsModel, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des paramètres de mise en page.
     */
    draw: function () {
        var close = this.close.bind(this);
        var content = '<div id="' + this.id + '" class="DescartesUIGpIsoCurve">';
        content += this.populateForm();
        content += '</div>';

        if (!_.isNil(this.formDialog)) {
            this.formDialog.dialog.modal('hide');
        }

        this.formDialog = new FormDialog({
            id: this.id + '_formDialog',
            title: this.getMessage('DIALOG_TITLE'),
            formClass: 'form-horizontal',
            btnCss: this.btnCss,
            otherBtnLabel: this.getMessage('SUBMIT_BUTTON'),
            content: content
        });

        this.formId = this.formDialog.id + '_formDialog';

        this.formDialog.open(close, close);
        document.getElementById(this.formId).querySelector("div[class='modal-footer']").querySelector("button[type='submit']").style = "display:none;";

        this.initDisplay();
        this.registerEvents(this.formId);
    },

    close: function () {
        this.formDialog = null;
        this.events.triggerEvent("efface");
    },

    CLASS_NAME: 'Descartes.UI.GeoPlateformeIsoCurveDialog'
});

module.exports = Class;
