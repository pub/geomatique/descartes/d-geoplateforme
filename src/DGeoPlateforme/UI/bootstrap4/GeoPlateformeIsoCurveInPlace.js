/* global Descartes */

var $ = require('jquery');
var _ = require('lodash');

var Utils = Descartes.Utils;
var AbstractInPlace = Descartes.UI.AbstractInPlace;

var AbstractGeoPlateformeIsoCurve = require('./AbstractGeoPlateformeIsoCurve');

/**
 * Class: Descartes.UI.GeoPlateformeIsoCurveInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie des paramètres pour le service d'isocurve de la GéoPlateforme'.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractInPlace>
 *
 * Evénements déclenchés:
 * choosed - Les paramètres sont saisis et valides.
 */
var Class = Utils.Class(AbstractInPlace, AbstractGeoPlateformeIsoCurve, {

    /**
     * Constructeur: Descartes.UI.GeoPlateformeIsoCurveInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.GeoPlateformeIsoCurveManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des paramètres d'impression.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceJQueryUI>).
     */
    initialize: function (div, paramsModel, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
        AbstractGeoPlateformeIsoCurve.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des paramètres de mise en page.
     */
    draw: function () {
        this.formId = this.id + '_form';
        var content = '<div id="' + this.id + '" class="DescartesUIGpIsoCurve">';
        content += '<form id="' + this.formId + '" class="form-horizontal">';
        content += this.populateForm();
        content += '</form>';
        content += '</div>';

        this.renderPanel(content);
        this.initDisplay();
        this.registerEvents(this.formId);

    },

    CLASS_NAME: 'Descartes.UI.GeoPlateformeIsoCurveInPlace'
});

module.exports = Class;
