/* global Descartes */

var _ = require('lodash');

var Utils = Descartes.Utils;

var geopf = {

    formatDuration: function (second) {
        return new Date(second * 1000).toISOString().substr(11, 8);
    },

    deleteChild: function (el) {
        while (el.firstChild) {
            el.removeChild(el.firstChild);
        }
    },

    convertTime: function (el) {
        var [hours, minutes, seconds] = el.split(':');
        var time = parseInt(hours, 10) * 3600 + parseInt(minutes, 10) * 60 + parseInt(seconds, 10);
        return time;
    }

};
_.extend(Utils, geopf);

module.exports = Utils;
