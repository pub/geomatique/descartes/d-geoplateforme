/* global Descartes*/

var _ = require('lodash');
var Utils = Descartes.Utils;
var DescartesPlug = require('./DGeoPlateforme/DGeoPlateforme');

Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Button.GeoPlateformeRoute');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Action.GeoPlateformeRouteManager');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Button.GeoPlateformeIsoCurve');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Action.GeoPlateformeIsoCurveManager');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Button.GeoPlateformeGetAltitude');
Descartes.Map._TOOLS_WITH_MAPCONTENT.push('Descartes.Action.GeoPlateformeGetAltitudeManager');

_.extend(Descartes.Action, DescartesPlug.Action);
_.extend(Descartes.Button, DescartesPlug.Button);
_.extend(Descartes.Layer, DescartesPlug.Layer);
_.extend(Descartes.UI, DescartesPlug.UI);
_.extend(Descartes.Messages, DescartesPlug.Messages);
_.extend(Descartes.Symbolizers, DescartesPlug.Symbolizers);

module.exports = Descartes;
